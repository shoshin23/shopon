//
//  DataLayer.swift
//  ShopOn
//
//  Created by Ilya Kuznetsov on 3/26/19.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation

class DataLayer {
    
    static let shared = DataLayer()
    
    let shoppingCart = ShoppingCart()
}
