//
//  AppStyle.swift
//

import UIKit

class AppStyle {
    
    static let orange = UIColor(hex: "F7CF5C")
    static let blue = UIColor(hex: "41A3EA")
    static let turquoise = UIColor(hex: "#55E3D5")
}

