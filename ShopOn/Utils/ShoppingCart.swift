//
//  ShoppingCart.swift
//  ShopOn
//
//  Created by Ilya Kuznetsov on 3/26/19.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation

class CartItem: NSObject {
    
    var name: String = ""
    var count: Int = 0
    var price: Double = 0
    
    var fullPrice: Double {
        return price * Double(count)
    }
}

class ShoppingCart: AMObservable {
    
    var items: [CartItem] = []
    
    func add(itemName: String, price: Double) {
        if let item = items.first(where: {
            return $0.name == itemName
        }) {
            item.count += 1
        } else {
            let item = CartItem()
            item.name = itemName
            item.price = price
            item.count = 1
            items.append(item)
        }
        post(nil)
    }
    
    func set(itemName: String, count: Int) {
        if let index = items.index(where: { $0.name == itemName }) {
            let item = items[index]
            item.count = count
            if item.count <= 0 {
                items.remove(at: index)
            }
            post(nil)
        }
    }
    
    func remove(itemName: String) {
        set(itemName: itemName, count: 0)
    }
}
