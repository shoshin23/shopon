//
//  CounterView.swift
//

import Foundation
import UIKit

class CounterView: UIView {
    
    @IBOutlet private var minus: UIButton!
    @IBOutlet private var plus: UIButton!
    @IBOutlet private var label: CounterField!
    
    var countObtainer: (()->(Int))! {
        didSet {
            reloadView()
        }
    }
    var changeCount: ((Int)->())!
    
    var product: Product! {
        didSet {
            reloadView()
            label.didChangeValue = { [unowned self] (count) in
                self.changeCount(count)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.didChangeValue = { [weak self] (value) in
            self?.changeCount(value)
        }
        label.backgroundColor = UIColor(hex: "#41A3EA")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.layer.cornerRadius = (self.frame.size.height - 10) / 2.0
    }
    
    private func reloadView() {
        guard let obtainer = countObtainer else {
            return
        }
        label.count = obtainer()
        minus.isEnabled = label.count > 1
        plus.isEnabled = label.count < 99
    }
    
    @IBAction private func plusAction(_ sender: UIButton) {
        changeCount(label.count + 1)
    }
    
    @IBAction private func minusAction(_ sender: UIButton) {
        changeCount(label.count - 1)
    }
}
