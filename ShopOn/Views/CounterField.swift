//
//  CounterField.swift
//

import Foundation
import UIKit

class CounterField: UIButton {
    
    var count: Int {
        set {
            let value = max(1, newValue)
            
            setTitle("\(value)", for: .normal)
            picker.selectRow(value - 1, inComponent: 0, animated: false)
        }
        get {
            if let text = title(for: .normal), !text.isEmpty {
                return Int(text)!
            }
            return 1
        }
    }
    
    var didChangeValue: ((Int)->())!
    private var picker = UIPickerView()
    
    open override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                alpha = 0.5
            } else if isEnabled {
                alpha = 1.0
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        picker.dataSource = self
        picker.delegate = self
        picker.backgroundColor = UIColor.clear
        
        addTarget(self, action: #selector(presentCounter), for: .touchUpInside)
    }
    
    @objc func presentCounter() {
        let viewController = UIViewController()
        viewController.modalPresentationStyle = .popover
        viewController.view = picker
        viewController.preferredContentSize = CGSize(width: 230, height: 180)
        viewController.popoverPresentationController?.delegate = self
        viewController.popoverPresentationController?.sourceView = self
        viewController.popoverPresentationController?.backgroundColor = UIColor.white
        viewController.popoverPresentationController?.sourceRect = self.bounds
        
        AppDelegate.shared().topViewController().present(viewController, animated: true, completion: nil)
    }
}

extension CounterField: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}

extension CounterField: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        didChangeValue(picker.selectedRow(inComponent: 0) + 1)
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        if let layer = popoverPresentationController.containerView?.layer {
            let animation = CABasicAnimation(keyPath: "shadowOpacity")
            animation.toValue = 0
            animation.duration = 0.5
            animation.isRemovedOnCompletion = false
            layer.add(animation, forKey: nil)
        }
        return true
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        if let layer = popoverPresentationController.containerView?.layer {
            layer.shadowOpacity = 0.15
            layer.shadowOffset = CGSize.zero
            layer.shadowRadius = 0.0
            layer.shadowPath = UIBezierPath(roundedRect: layer.bounds, cornerRadius: 0).cgPath
        }
    }
}
