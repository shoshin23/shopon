//
//  BaseTableViewCell.swift
//  ShopOn
//
//  Created by Ilya Kuznetsov on 5/16/19.
//  Copyright © 2019 Envision. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let counter = CounterView.loadFromNib()
    var deleteAction: (()->())?
    var didSelectAction: (()->())?
    
    let itemLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: "#383838")
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.numberOfLines = 0
        return label
    }()

    let scrollView = UIScrollView()
    let containerView = UIView()
    let deleteButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .red
        button.setTitle("Delete", for: .normal)
        button.tintColor = .white
        return button
    }()
    
    func setupViews() {
        backgroundColor = .clear
        
        contentView.setupShadow()
        scrollView.backgroundColor = .white
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        contentView.addGestureRecognizer(scrollView.panGestureRecognizer)
        containerView.backgroundColor = .white
        scrollView.layer.cornerRadius = 5
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(deleteButton)
        
        contentView.addSubview(scrollView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(containerView)
        
        containerView.addSubview(itemLabel)
        counter.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(counter)
        
        scrollView.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor, paddingTop: 2, paddingLeading: 15, paddingBottom: 2, paddingTrailing: 15, width: nil, height: nil)
        
        containerView.anchor(top: contentView.topAnchor, leading:nil, bottom: contentView.bottomAnchor, trailing: nil, paddingTop: 2, paddingLeading: 15, paddingBottom: 2, paddingTrailing: 15, width: nil, height: nil)
        containerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        containerView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        
        containerView.topAnchor.constraint(equalTo: deleteButton.topAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: deleteButton.leftAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: deleteButton.bottomAnchor).isActive = true
        deleteButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        let redView = UIView()
        redView.translatesAutoresizingMaskIntoConstraints = false
        redView.backgroundColor = deleteButton.backgroundColor
        scrollView.addSubview(redView)
        deleteButton.topAnchor.constraint(equalTo: redView.topAnchor).isActive = true
        deleteButton.rightAnchor.constraint(equalTo: redView.leftAnchor).isActive = true
        deleteButton.bottomAnchor.constraint(equalTo: redView.bottomAnchor).isActive = true
        deleteButton.addTarget(self, action: #selector(deleteButtonAction), for: .touchUpInside)
        redView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        counter.anchorCenterYToSuperview()
        counter.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        reloadScrollContentSize()
        
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectButtonAction)))
    }
    
    @objc private func deleteButtonAction() {
        deleteAction?()
    }
    
    @objc private func selectButtonAction() {
        didSelectAction?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.reloadScrollContentSize()
        }
    }
    
    private func reloadScrollContentSize() {
        scrollView.contentSize = scrollView.frame.size
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 70)
    }
}
