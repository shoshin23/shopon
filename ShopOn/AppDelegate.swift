//
//  AppDelegate.swift
//  ShopOn
//
//  Created by Benjamin on 25/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
    
    static func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func topViewController() -> UIViewController {
        var topVC = window!.rootViewController!
        while topVC.presentedViewController != nil {
            topVC = topVC.presentedViewController!
        }
        return topVC
    }
}

