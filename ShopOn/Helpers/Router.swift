//
//  Router.swift
//  ShopOn
//
//  Created by Benjamin on 10/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation
import Alamofire

var appDelegate = UIApplication.shared.delegate as? AppDelegate

enum Router: URLRequestConvertible {
    
    static let baseURLString = "https://www.buycott.com/api/v4/products/"
    static var OAuthToken: String?
    
    case getProductsWithAccessToken (barcode: String, access_token: String)
    case getProducts (barcode: String)
    
    // type of HTTPMethod
    var method: Alamofire.HTTPMethod {
        switch self {
        case .getProductsWithAccessToken:
            return .post
            
        case .getProducts:
            return .post
        }
    }
    
    // add path string here
    var path: String {
        switch self {
        case .getProductsWithAccessToken:
            return "lookup"
        case .getProducts:
            return "lookup"
        }
    }
    
    // parameters
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
            
        case .getProductsWithAccessToken(let barcode, let access_token):
            let parameters = ["barcode": barcode, "access_token": access_token]
            return try URLEncoding.default.encode(urlRequest, with: parameters)
            
        case .getProducts(let barcode):
            let parameters = ["barcode": barcode]
            return try URLEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}
