//
//  APIManager.swift
//  ShopOn
//
//  Created by Benjamin on 10/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIManager: NSObject {
    
    // Shared Instance
    static let shared : APIManager = {
        let instance = APIManager()
        return instance
    }()
    
    // Call Api with request
    func request( request: URLRequestConvertible, Selector selector: UIViewController, AndSuccessHandler successHandler:@escaping (_ responseJSON: JSON?) -> Void, AndFailureHandler failureHandler:@escaping (_ responseError: Error) -> Void) {
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success(let result):
                let json = JSON.init(result)
                successHandler(json)
            case .failure(let error):
                failureHandler(error)
            }
        }
    }
}
