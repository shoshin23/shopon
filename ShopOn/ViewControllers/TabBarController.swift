//
//  TabBarController.swift
//  ShopOn
//
//  Created by Ilya Kuznetsov on 3/21/19.
//  Copyright © 2019 Envision. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override var selectedViewController: UIViewController? {
        didSet {
            let colors = [AppStyle.orange, AppStyle.blue, AppStyle.turquoise]
            if selectedIndex < colors.count {
                tabBar.tintColor = colors[selectedIndex]
            } else {
                tabBar.tintColor = AppStyle.blue
            }
        }
    }
}
