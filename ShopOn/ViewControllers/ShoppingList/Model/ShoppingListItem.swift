//
//  ShoppingListItem.swift
//  ShopOn
//
//  Created by Benjamin on 19/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation

class ShoppingListItem: NSObject {
    var name: String = ""
    var amount: Int = 1
}
