//
//  ShoppingListSectionHeader.swift
//  ShopOn
//
//  Created by Benjamin on 28/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import Foundation
import UIKit

class ShoppingListSectionHeader: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    let sectionLabel: UILabel = {
        let label = UILabel()
        label.text = "Dairy"
        label.textColor = UIColor(hex: "#858E98")
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.adjustsFontForContentSizeCategory = true
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        setupViews()
    }
    
    func setupViews() {
        addSubview(sectionLabel)
        sectionLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, paddingTop: 0, paddingLeading: 30, paddingBottom: 7, paddingTrailing: 30, width: nil, height: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
