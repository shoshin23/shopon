//
//  ShoppingListItemCell.swift
//  ShopOn
//
//  Created by Benjamin on 25/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import Foundation
import UIKit

class ShoppingListItemCell: BaseTableViewCell {
    
    let tickContainer: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor(hex: "#55E3D5")
        view.layer.cornerRadius = 5
        return view
    }()
    
    let tickImageView: UIImageView = {
       let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Tick")
        return iv
    }()
    
    lazy var itemLabelLeadingAnchorToContainer = itemLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15)
    lazy var itemLabelLeadingAnchorToTickView = itemLabel.leadingAnchor.constraint(equalTo: tickContainer.trailingAnchor, constant: 15)
    lazy var itemLabelTrailingAnchorToNumberOfItemsContainerView = itemLabel.trailingAnchor.constraint(equalTo: counter.leadingAnchor, constant: -15)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var showsDoneMark: Bool = false {
        didSet {
            if showsDoneMark {
                containerView.addSubview(tickContainer)
                tickContainer.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: nil, paddingTop: 0, paddingLeading: 0, paddingBottom: 0, paddingTrailing: 0, width: 52, height: nil)
                
                tickContainer.addSubview(tickImageView)
                tickImageView.anchorCenterXToSuperview()
                tickImageView.anchorCenterYToSuperview()
            } else {
                tickContainer.removeFromSuperview()
            }
            itemLabelLeadingAnchorToContainer.isActive = !showsDoneMark
            itemLabelLeadingAnchorToTickView.isActive = showsDoneMark
        }
    }
    
    var item: ShoppingListItem! {
        didSet {
            let item = self.item!
            itemLabel.text = item.name
            counter.countObtainer = {
                return item.amount
            }
            counter.changeCount = { [weak self] (value) in
                item.amount = value
                self?.counter.countObtainer = self?.counter.countObtainer
            }
        }
    }
    
    override func setupViews() {
        super.setupViews()
        
        itemLabel.anchor(top: containerView.topAnchor, leading: nil, bottom: containerView.bottomAnchor, trailing: nil, paddingTop: 15, paddingLeading: 0, paddingBottom: 15, paddingTrailing: 0, width: nil, height: nil)
        
        itemLabelLeadingAnchorToContainer.isActive = true
        itemLabelTrailingAnchorToNumberOfItemsContainerView.isActive = true
    }
}
