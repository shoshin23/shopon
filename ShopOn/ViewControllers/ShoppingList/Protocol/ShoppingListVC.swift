//
//  ShoppingListVC.swift
//  ShopOn
//
//  Created by Benjamin on 28/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import Foundation
import UIKit

protocol ShoppingListVC: AnyObject where Self: UIViewController {
    func processRecognitionResults(_ resultsString: String)
}
