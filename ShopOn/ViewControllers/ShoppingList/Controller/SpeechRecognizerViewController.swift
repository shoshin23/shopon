//
//  SpeechRecognizerViewController.swift
//  ShopOn
//
//  Created by Benjamin on 27/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import Foundation
import UIKit
import Speech

class SpeechRecognizerViewController: UIViewController {
    @IBOutlet weak var recognizedWordsLabel: UILabel!
    
    weak var shoppingListVC: ShoppingListVC!
    var resultsString: String = ""
    
    // TODO: Add other locales
    private let audioEngine = AVAudioEngine()
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest? = SFSpeechAudioBufferRecognitionRequest()
    private var recognitionTask: SFSpeechRecognitionTask?
    private var isRecording = false
    var speechTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAudioSession()
        recordAndRecognizeSpeech()
        isRecording = true
        startSpeechTimerFirstTime()
    }
    
    deinit {
        print("deinit")
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        invalidateSpeechTimer()
        finishRecording()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        invalidateSpeechTimer()
        finishRecordingAndDismiss()
    }
    
    private func startSpeechTimerFirstTime() {
        speechTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(finishRecordingAndDismiss), userInfo: nil, repeats: false)
    }
    
    private func restartSpeechTimerInContinueMode() {
        invalidateSpeechTimer()
        speechTimer = Timer.scheduledTimer(timeInterval: 1.75, target: self, selector: #selector(finishRecordingAndDismiss), userInfo: nil, repeats: false)
    }
    
    private func invalidateSpeechTimer() {
        speechTimer.invalidate()
        speechTimer = Timer()
    }
    
    private func configureAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch { }
    }
    
    @objc private func finishRecordingAndDismiss() {
        finishRecording()
        shoppingListVC.processRecognitionResults(resultsString)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func finishRecording() {
        isRecording = false
        recognitionTask?.finish()
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        
        self.recognitionRequest = nil
        self.recognitionTask = nil
        
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch { }
    }
    
    private func recordAndRecognizeSpeech() {
        requestTranscribePermissionsIfRequired()
        
        let node = audioEngine.inputNode
        let recordingFormat = node.inputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { [unowned self] buffer, _ in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            showErrorAlert("There has been an audio engine error.")
            return
        }
        
        guard let speechRecognizer = speechRecognizer else {
            showErrorAlert("Speech recognition is not supported for your current locale.")
            return
        }
        
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { [weak self] result, error in
            guard let isRecording = self?.isRecording, isRecording == true else { return }
            self?.restartSpeechTimerInContinueMode()
            if let result = result {
                print(result)
                let bestString = result.bestTranscription.formattedString
                self?.recognizedWordsLabel.text = bestString
                self?.resultsString = bestString
            } else if let error = error {
                guard let task = self?.recognitionTask, !task.isFinishing else { return }
                self?.showErrorAlert("There has been a speech recognition error.")
                print(error.localizedDescription)
            }
        })
    }
    
    private func requestTranscribePermissionsIfRequired() {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .authorized:
            return
        case .restricted:
            showErrorAlert("The functionality is not available on this device.")
        case .denied:
            showErrorAlert("You've denied granting the access to the microphone before. Please change it in the Setting to proceed.")
        case .notDetermined:
            SFSpeechRecognizer.requestAuthorization { [unowned self] authStatus in
                DispatchQueue.main.async {
                    if authStatus == .authorized {
                        print("Good to go!")
                        self.recordAndRecognizeSpeech()
                    } else {
                        print("Transcription permission was declined.")
                    }
                }
            }
        }
    }
}
