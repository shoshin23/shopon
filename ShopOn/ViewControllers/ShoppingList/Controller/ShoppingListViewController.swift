//
//  ShoppingListViewController.swift
//  ShopOn
//
//  Created by Benjamin on 25/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import Foundation
import UIKit
import Speech
import NaturalLanguage

class ShoppingListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ShoppingListVC {
    
    var data = [FoodType: [ShoppingListItem]]()
    var nonEmptySections = [FoodType]()
    var selectedItems = Set<ShoppingListItem>()
    
    @IBOutlet weak var mainContainerView: UIView! {
        didSet {
            mainContainerView.backgroundColor = UIColor(hex: "#F6F6F6")
        }
    }
    @IBOutlet weak var addItemButton: UIButton! {
        didSet {
            addItemButton.setTitleColor(UIColor(hex: "#383838"), for: .normal)
        }
    }
    @IBOutlet weak var shoppingListTableView: UITableView! {
        didSet {
            shoppingListTableView.backgroundColor = .clear
            shoppingListTableView.separatorStyle = .none
        }
    }
    @IBOutlet weak var micViewContainer: UIView!
    
    let tableFooterView: UIView = {
       let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 100))
        view.backgroundColor = .clear
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(microphoneButtonTapped))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        micViewContainer.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func addItemButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Add Item", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak alert, self] (_) in
            if let input = alert?.textFields![0].text, !input.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                if let item = RecognizableFoodItem(rawValue: input) {
                    self.addItemToAppropriateSection(item, amount: 1)
                } else {
                    self.addUnrecognizedItem(input, amount: 1)
                }
                self.shoppingListTableView.reloadData()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func microphoneButtonTapped() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SpeechRecognizerViewController") as! SpeechRecognizerViewController
        vc.shoppingListVC = self
        present(vc, animated: true, completion: nil)
    }
    
    private func setupTableView() {
        shoppingListTableView.register(ShoppingListItemCell.self, forCellReuseIdentifier: ShoppingListItemCell.reuseIdentifier)
        shoppingListTableView.delegate = self
        shoppingListTableView.dataSource = self
        shoppingListTableView.alwaysBounceVertical = true
        shoppingListTableView.tableFooterView = tableFooterView
        shoppingListTableView.sectionHeaderHeight = UITableView.automaticDimension
        shoppingListTableView.estimatedSectionHeaderHeight = 25
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        nonEmptySections.removeAll()
        data.forEach { key, _ in
            nonEmptySections.append(key)
        }
        return data.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count > 0 {
            shoppingListTableView.backgroundView = nil
            let section = nonEmptySections[section]
            guard let items = data[section] else {
                fatalError("Number of sections or data is incorrect.")
            }
            return items.count
        } else {
            showNoDataLabel()
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = shoppingListTableView.dequeueReusableCell(withIdentifier: ShoppingListItemCell.reuseIdentifier, for: indexPath) as! ShoppingListItemCell
        let section = nonEmptySections[indexPath.section]
        guard let items = data[section] else {
            fatalError("Number of sections or data is incorrect.")
        }
        let item = items[indexPath.item]
        cell.item = item
        cell.showsDoneMark = selectedItems.contains(cell.item)
        
        cell.deleteAction = { [weak self] in
            
            if let wSelf = self {
                let section = wSelf.nonEmptySections[indexPath.section]
                guard let items = wSelf.data[section] else {
                    fatalError()
                }
                if items.count > 1 {
                    wSelf.data[section]?.remove(at: indexPath.item)
                    tableView.beginUpdates()
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.endUpdates()
                } else {
                    wSelf.data.removeValue(forKey: section)
                    if let index = wSelf.nonEmptySections.index(of: section) {
                        wSelf.nonEmptySections.remove(at: index)
                        tableView.beginUpdates()
                        tableView.deleteRows(at: [indexPath], with: .fade)
                        tableView.deleteSections([index], with: .fade)
                        tableView.endUpdates()
                    }
                }
            }
        }
        
        cell.didSelectAction = { [weak self] in
            
            if let wSelf = self {
                if wSelf.selectedItems.contains(item) {
                    wSelf.selectedItems.remove(item)
                    cell.showsDoneMark = false
                } else {
                    wSelf.selectedItems.insert(item)
                    cell.showsDoneMark = true
                }
                cell.layoutIfNeeded()
                UIView.performWithoutAnimation {
                    tableView.beginUpdates()
                    tableView.endUpdates()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        shoppingListTableView.deselectRow(at: indexPath, animated: false)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ShoppingListSectionHeader()
        headerView.sectionLabel.text = nonEmptySections[section].rawValue.localizedCapitalized
        return headerView
    }
    
    private func showNoDataLabel() {
        let noDataBackgroundView = UIView()
        let containerView = UIView()
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "NoItemsInShoppingList")
        let label = UILabel()
        label.text = "Your shopping list is empty"
        label.numberOfLines = 0
        
        noDataBackgroundView.addSubview(containerView)
        containerView.addSubview(imageView)
        containerView.addSubview(label)
        
        containerView.anchorCenterXToSuperview()
        containerView.anchorCenterYToSuperview()
        
        imageView.anchorCenterXToSuperview()
        imageView.anchor(top: containerView.topAnchor, leading: nil, bottom: nil, trailing: nil, paddingTop: 0, paddingLeading: 0, paddingBottom: 0, paddingTrailing: 0, width: nil, height: nil)
        label.anchor(top: imageView.bottomAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, paddingTop: 20, paddingLeading: 0, paddingBottom: 0, paddingTrailing: 0, width: nil, height: nil)
        label.widthAnchor.constraint(lessThanOrEqualToConstant: shoppingListTableView.frame.width * 0.7).isActive = true
        
        shoppingListTableView.backgroundView = noDataBackgroundView
    }
    
    // MARK: - Processing words from speech recognition
    enum FoodType: String, CaseIterable {
        case dairy
        case fruit
        case vegetables
        case dryBakingGoods = "Dry Baking Goods"
        case other
    }
    
    enum RecognizableFoodItem: String {
        case apple
        case banana
        case onion
        case potato
        case milk
        case eggs
        case yoghurt
        case spaghetti
        case basmatiRice = "basmati"
    }
    
    func processRecognitionResults(_ string: String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .spellOut
        
        let tagger = NLTagger(tagSchemes: [.lexicalClass])
        let options: NLTagger.Options = [.omitPunctuation, .omitWhitespace, .joinNames]
        let tags: [NLTag] = [.number]
        
        let string = string.lowercased()
        let wordCombinations = string.components(separatedBy: "and")
        
        for var combination in wordCombinations {
            print(combination)
            
            var amount = 1
            
            tagger.string = combination
            tagger.enumerateTags(in: combination.startIndex..<combination.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
                print(tag)
                
                switch combination[tokenRange] {
                case "to":
                    amount = 2
                    removeNumberWordFrom(&combination, numberRange: tokenRange)
                    return false
                case "for":
                    amount = 4
                    removeNumberWordFrom(&combination, numberRange: tokenRange)
                    return false
                case "five":
                    amount = 5
                    removeNumberWordFrom(&combination, numberRange: tokenRange)
                    return false
                default:
                    break
                }
                
                if let number = Int(combination[tokenRange]) {
                    print("\(combination[tokenRange])")
                    amount = number
                    removeNumberWordFrom(&combination, numberRange: tokenRange)
                    return false
                }
                
                if let tag = tag, tags.contains(tag) {
                    print("\(combination[tokenRange]): \(tag.rawValue)")
                    if let number = formatter.number(from: String(combination[tokenRange]))?.intValue {
                        amount = number
                        removeNumberWordFrom(&combination, numberRange: tokenRange)
                        return false
                    }
                }
                return true
            }
            
            guard combination.trimmingCharacters(in: .whitespaces) != "" else { continue }
            if let item = RecognizableFoodItem(rawValue: combination) {
                addItemToAppropriateSection(item, amount: amount)
            } else {
                addUnrecognizedItem(combination, amount: amount)
            }
        }
        shoppingListTableView.reloadData()
    }
    
    private func removeNumberWordFrom(_ string: inout String, numberRange: Range<String.Index>) {
        string.removeSubrange(numberRange)
        print(string)
        let components = string.components(separatedBy: .whitespacesAndNewlines)
        string = components.filter { !$0.isEmpty }.joined(separator: " ")
        print(string)
        return
    }
    
    private func addUnrecognizedItem(_ name: String, amount: Int) {
        let item = ShoppingListItem()
        item.name = name
        item.amount = amount
        data[.other] == nil ? data[.other] = [item] : data[.other]?.append(item)
    }
    
    private func addItemToAppropriateSection(_ item: RecognizableFoodItem, amount: Int) {
        let shoppingListItem = ShoppingListItem()
        shoppingListItem.name = item.rawValue
        shoppingListItem.amount = amount
        let section = determineSectionFor(item)
        data[section] == nil ? data[section] = [shoppingListItem] : data[section]?.append(shoppingListItem)
    }
    
    private func determineSectionFor(_ item: RecognizableFoodItem) -> FoodType {
        switch item {
        case .apple, .banana:
            return .fruit
        case .onion, .potato:
            return .vegetables
        case .milk, .eggs, .yoghurt:
            return .dairy
        case .spaghetti, .basmatiRice:
            return .dryBakingGoods
        }
    }
    
    deinit {
        print("ShoppingList deinit")
    }
}
