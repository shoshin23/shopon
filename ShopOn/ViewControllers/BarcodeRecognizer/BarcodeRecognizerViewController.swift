//
//  BarcodeRecognizerViewController.swift
//  ShopOn
//
//  Created by Benjamin on 10/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Vision
import SwiftySound
import SwiftLinkPreview

class BarcodeRecognizerViewController: UIViewController {
    @IBOutlet weak var barcodeTabMenuButton: UIButton! {
        didSet {
            barcodeTabMenuButton.setTitleColor(UIColor(hex: "#F6BE22"), for: .normal)
        }
    }
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var predictionView: UIView! {
        didSet {
            predictionView.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var predictionLabel: UILabel!
    @IBOutlet weak var addToBasketButton: UIButton! {
        didSet {
            addToBasketButton.layer.cornerRadius = 25
            addToBasketButton.setTitleColor(UIColor(hex: "#383838"), for: .normal)
            addToBasketButton.backgroundColor = UIColor(hex: "#F6BE22")
        }
    }
    @IBOutlet weak var continueScanningButton: UIButton! {
        didSet {
            continueScanningButton.layer.cornerRadius = 25
            continueScanningButton.setTitleColor(UIColor(hex: "#383838"), for: .normal)
            continueScanningButton.backgroundColor = .lightGray
        }
    }
    
    var audioPlayer = AVAudioPlayer()
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    let captureDevice = AVCaptureDevice.default(for: .video)
    var queue: DispatchQueue = DispatchQueue(label: "com.shopOn.queue")
    var timer = Timer()
    var detectBarcodeFlag = false
    var barcodeVisibleCounter = 0
    var barcodeString: String!
    let synthesizer = AVSpeechSynthesizer()
    
    var lastRecognizedProduct: Product?
    
    let currentLang = "en"
    
    let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        predictionView.isHidden = true
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            let videoDataOutput = AVCaptureVideoDataOutput()
            videoDataOutput.setSampleBufferDelegate(self, queue: queue)
            videoDataOutput.videoSettings = [String(kCVPixelBufferPixelFormatTypeKey): Int(kCVPixelFormatType_32BGRA)]
            captureSession?.addOutput(videoDataOutput)
        } catch {
            showErrorAlert("Unable to start the camera!")
        }
        
        if captureSession != nil {
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.frame = view.bounds
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            previewView.layer.addSublayer(videoPreviewLayer!)
            captureSession?.startRunning()
        } else {
            showErrorAlert("Unable to start the camera!")
            return
        }
        
        detectBarcodeFlag = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        captureSession?.stopRunning()
        self.detectBarcodeFlag = false
        predictionView.isHidden = true
        lastRecognizedProduct = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        captureSession?.startRunning()
        self.detectBarcodeFlag = true
    }
    
    @IBAction func shoppingListTabMenuButtonTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func addToBasketButtonTapped(_ sender: Any) {
        print("add to basket button tapped")
        guard let item = lastRecognizedProduct else {
            assertionFailure("The button should only be visible if there is a product recognized.")
            return
        }
        
        guard let viewControllers = tabBarController?.viewControllers else {
            assertionFailure()
            return
        }
        
        for vc in viewControllers {
            if vc is ShoppingCartViewController {
                _ = vc.view
                
                print(item)
                let price = getPriceFor(item)
                DataLayer.shared.shoppingCart.add(itemName: item.name, price: Double(price)!)
            }
        }
        
        predictionView.isHidden = true
        //add alert here
        // the alert view
        let alert = UIAlertController(title: "", message: "Product added to shopping cart.", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 4
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
            self.detectBarcodeFlag = true
        }
        
    }
    
    @IBAction func continueScanningButtonTapped(_ sender: Any) {
        predictionView.isHidden = true
        detectBarcodeFlag = true
        lastRecognizedProduct = nil
    }
    
    deinit {
        print("Barcode deinit")
    }
    
    private func getPriceFor(_ product: Product) -> String {
        switch product.barcode {
        case "7613036249928":
            return "25.90"
        default:
            return String(format: "%.2f", Double.random(in: 0.01...9.99))
        }
    }
    
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        audioPlayer.rate = 0.5
        audioPlayer.volume = 0.8 //only for the processing sound.
        audioPlayer.play()
    }
    
    func detectBarcodes(img: UIImage) {
        guard let textCIImage = CIImage(image:img) else {
            return
        }
        if #available(iOS 11.0, *) {
            let barcodeDetectionRequest = VNDetectBarcodesRequest(completionHandler: self.handBarcodeRequests)
            let requestHandler = VNImageRequestHandler(cgImage: img.cgImage!, options: [:])
            do {
                try requestHandler.perform([barcodeDetectionRequest])
            } catch {
                print(error)
            }
        } else {
            let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
            let ciBarcodeDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: accuracy)
            let barcodes = ciBarcodeDetector?.features(in: textCIImage)
            if (barcodes?.isEmpty)! {
                print("No barcodes found. Dont bother.")
                return  //no text found. dont bother calling Amazon.
            } else {
                barcodeVisibleCounter += 1
                self.playBarcodeDetectionSound()
                
                if barcodeVisibleCounter >= 10 {
                    self.barcodeVisibleCounter = 0
                    //call the goddamn api.
                    playBarcodeSuccessSound()
                    getProductsAPI((barcodes?.first as! CIQRCodeFeature).messageString!, access_token: "0d4IttQC8lSdzR-hao8uPB3ezTRh0AgkZ9OhKXDL")
                    scheduledTimerWithTimeInterval()
                    self.detectBarcodeFlag = false
                }
            }
        }
    }
    
    @available(iOS 11.0, *)
    func handBarcodeRequests(request: VNRequest, errror: Error?)  {
        guard let observations = request.results as? [VNBarcodeObservation] else {
            fatalError("unexpected result type!")
        }
        if observations.isEmpty {
            print("No barcodes")
        } else {
            self.barcodeVisibleCounter += 1
            print("barcode found")
            self.playBarcodeDetectionSound()
            
            if barcodeVisibleCounter >= 10 {
                if (observations.last?.symbology == .QR) {
                    print("DETECTED QR CODE and this is the payload:")
                    print(observations.last?.payloadStringValue)
                    getQRCode((observations.last?.payloadStringValue)!)
                    playBarcodeSuccessSound()
                    self.detectBarcodeFlag = false
                    return
                }
                print(observations.last?.barcodeDescriptor)
                print(observations.last?.symbology.rawValue)
                print(observations.last?.payloadStringValue)
                self.barcodeVisibleCounter = 0
                //call the goddamn api.
                playBarcodeSuccessSound()
                getProductsAPI((observations.last?.payloadStringValue)!, access_token: "0d4IttQC8lSdzR-hao8uPB3ezTRh0AgkZ9OhKXDL")
                scheduledTimerWithTimeInterval()
                self.detectBarcodeFlag = false
            }
        }
    }
    
    func getQRCode(_ qrCode:String) {
        let url = qrCode
        
        slp.preview(url,
                    onSuccess: { result in
                        DispatchQueue.main.async {
                            self.barcodeString = qrCode
                            self.showResultsFor(product: result[.title] as! String)
                            UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.predictionLabel)
                            if !UIAccessibility.isVoiceOverRunning {
                                self.speak(string: result[.title] as! String, language: String(self.currentLang))
                            }
                            
                        } },
                    onError: { error in
                        self.showFailure(message: "This is not a valid barcode.")
                        if !UIAccessibility.isVoiceOverRunning {
                            self.speak(string: "This is not a valid barcode.", language: String(self.currentLang))
                        }
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.predictionLabel)
        })
        
        
    }
    
    func getProductsAPI(_ barcode:String, access_token:String) {
        let request = Router.getProductsWithAccessToken(barcode: barcode, access_token: access_token)
        print(request.urlRequest?.url ?? "")
        APIManager.shared.request(request: request, Selector: self, AndSuccessHandler: { response in
            print(response ?? "Response is nil")
            guard let result = response else { return }
            
            let success = result["success"].boolValue
            let statusCode = result["status_code"]
            print("status code is >> \(statusCode)")
            if success {
                let productCount = result["status_code"].intValue
                if productCount > 0 {
                    
                    // retrieve array of product dictionary
                    let products = result["products"].arrayValue
                    for product in products {
                        // play with data of product
                        let productName = product["product_name"].stringValue
                        print("product name  is >> \(productName)")
                        let brandName = product["brand_name"].stringValue
                        print("brand_name  is >> \(brandName)")
                        let countryOfOrigin = product["country_of_origin"].stringValue
                        print("country of origin  is >> \(countryOfOrigin)")
                        let productImageUrl = product["product_image_url"].stringValue
                        print("product image url  is >> \(productImageUrl)")
                        self.lastRecognizedProduct = Product.init(barcode: barcode, name: productName, brand: brandName, countryOfOrigin: countryOfOrigin, imageURL: productImageUrl)
                        DispatchQueue.main.async {
                            //                                self.displayBarcodePopup(name: brand_name+" "+product_name)
                            
                            self.barcodeString = brandName+" "+productName
                            self.showResultsFor(product: brandName+" "+productName)
                            UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.predictionLabel)
                            if !UIAccessibility.isVoiceOverRunning {
                                self.speak(string: brandName+" "+productName, language: String(self.currentLang))
                            }
                            
                        }
                    }
                    
                } else {
                    // tackle if products count is 0
                    DispatchQueue.main.async {
                        self.showFailure(message: "Product not found.")
                        if !UIAccessibility.isVoiceOverRunning {
                            self.speak(string: "Product not found.", language: String(self.currentLang))
                        }
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.predictionLabel)
                        
                    }
                }
                
            } else {
                // tackle when failure occurs
                print("Failure")
                print(result["message"])
                DispatchQueue.main.async {
                    if result["message"].stringValue == "Product not found" {
                        self.showFailure(message: "Product not found.")
                        if !UIAccessibility.isVoiceOverRunning {
                            self.speak(string: "Product not found.", language: String(self.currentLang))
                        }
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.predictionLabel)
                    }
                    else if result["message"].stringValue == "This is not a valid barcode" {
                        self.showFailure(message: "This is not a valid barcode")
                        if !UIAccessibility.isVoiceOverRunning {
                            self.speak(string: "This is not a valid barcode", language: String(self.currentLang))
                        }
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.predictionLabel)
                    }
                }
            }
        }, AndFailureHandler: { error in
            print(error.localizedDescription )
        })
    }
    
    func playBarcodeDetectionSound() {
        Sound.category = .playback
        Sound.enabled = true
        Sound.play(file: "begin_record", fileExtension: "caf", numberOfLoops: 0)
    }
    
    func playBarcodeSuccessSound() {
        Sound.category = .playback
        Sound.enabled = true
        Sound.play(file: "Barcode", fileExtension: "wav", numberOfLoops: 0)
    }
}

// MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
extension BarcodeRecognizerViewController : AVCaptureVideoDataOutputSampleBufferDelegate {
    func getImageFromSampleBuffer(buffer: CMSampleBuffer) -> UIImage? {
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer) {
            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let context = CIContext()
            let imageRect = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer))
            
            if let image = context.createCGImage(ciImage, from: imageRect) {
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: .right)
            }
        }
        return nil
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        if let image = self.getImageFromSampleBuffer(buffer: sampleBuffer) {
            if detectBarcodeFlag {
                self.detectBarcodes(img: image)
            }
        }
    }
    
    func showResultsFor(product: String) {
        predictionView.isHidden = false
        addToBasketButton.isHidden = false
        continueScanningButton.isHidden = false
        predictionLabel.text = product
    }
    
    func showFailure(message: String) {
        predictionLabel.text = message
        addToBasketButton.isHidden = true
        continueScanningButton.isHidden = true
        predictionView.isHidden = false
        detectBarcodeFlag = true
    }
    
    func speak(string:String, language:String) {
        let utterance = AVSpeechUtterance(string: string)
        utterance.voice = AVSpeechSynthesisVoice(identifier: "en")
        synthesizer.speak(utterance)
    }
    
    func detectedLangauge<T: StringProtocol>(_ forString: T) -> String? {
        if #available(iOS 11.0, *) {
            guard let languageCode = NSLinguisticTagger.dominantLanguage(for: String(forString)) else {
                return nil
            }
            return languageCode
        } else {
            return AVSpeechSynthesisVoice.currentLanguageCode()
        }
    }
}
