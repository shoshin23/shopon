//
//  Product.swift
//  ShopOn
//
//  Created by Benjamin on 18/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import Foundation

struct Product {
    let barcode: String
    let name: String
    let brand: String
    let countryOfOrigin: String
    let imageURL: String
}
