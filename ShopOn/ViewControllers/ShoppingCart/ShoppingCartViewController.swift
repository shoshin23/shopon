//
//  ShoppingCartViewController.swift
//  ShopOn
//
//  Created by Benjamin on 18/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var shoppingCartTableView: UITableView!
    @IBOutlet weak var totalAmountLabel: UILabel?
    @IBOutlet weak var payAndGoButton: UIButton! {
        didSet {
            payAndGoButton.layer.cornerRadius = 25
            payAndGoButton.backgroundColor = UIColor(hex: "#71F5E8")
            payAndGoButton.setTitleColor(UIColor(hex: "#383838"), for: .normal)
            payAndGoButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        }
    }
    
    var data = [CartItem]() {
        didSet {
            totalAmountPayable = data.reduce(0, { (result, item) -> Double in
                result + item.fullPrice
            })
            
            let value = data.reduce(0, { (result, item) -> Int in
                result + item.count
            })
            tabBarItem.badgeValue = value > 0 ? "\(value)" : nil
            totalAmountLabel?.text = "Total: \(String(format: "%.2f", totalAmountPayable)) €"
        }
    }
    var totalAmountPayable: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
        DataLayer.shared.shoppingCart.observe(self) { [weak self] (_) in
            self?.data = DataLayer.shared.shoppingCart.items
            self?.shoppingCartTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        shoppingCartTableView.reloadData()
        totalAmountLabel?.text = "Total: \(String(format: "%.2f", totalAmountPayable)) €"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // FIXME: Doesn't work!
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.shoppingCartTableView)
    }
    
    @IBAction func payAndGoButtonTapped(_ sender: Any) {
        guard data.count > 0 else {
            let alert = UIAlertController(title: "Oops!", message: "Please add at least one item to pay for.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            return
        }
        let alert = UIAlertController(title: "Success!", message: "You've just successfully paid for the items in your shopping cart. Thank you!", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { [weak self] alert in
            self?.removeAllItems()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    private func removeAllItems() {
        data.removeAll()
        shoppingCartTableView.reloadData()
    }
    
    private func setupTableView() {
        shoppingCartTableView.register(ShoppingCartCell.self, forCellReuseIdentifier: ShoppingCartCell.reuseIdentifier)
        shoppingCartTableView.delegate = self
        shoppingCartTableView.dataSource = self
        shoppingCartTableView.alwaysBounceVertical = false
        shoppingCartTableView.tableFooterView = UIView()
        shoppingCartTableView.separatorStyle = .none
        shoppingCartTableView.allowsSelection = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ShoppingCartCell.reuseIdentifier, for: indexPath) as? ShoppingCartCell else {
            fatalError("Incorrect cell class")
        }
        let item = data[indexPath.item]
        
        cell.itemLabel.text = item.name
        cell.priceLabel.text = "\(item.price) €"
        cell.counter.changeCount = { (value) in
            DataLayer.shared.shoppingCart.set(itemName: item.name, count: value)
        }
        cell.counter.countObtainer = {
            return item.count
        }
        cell.deleteAction = { [weak self] in
            if let wSelf = self {
                wSelf.data.remove(at: indexPath.item)
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.endUpdates()
            }
        }
        return cell
    }
}
