//
//  ShoppingCartCell.swift
//  ShopOn
//
//  Created by Benjamin on 18/01/2019.
//  Copyright © 2019 Envision. All rights reserved.
//

import UIKit

class ShoppingCartCell: BaseTableViewCell {
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: "#686868")
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        
        containerView.addSubview(priceLabel)
        
        itemLabel.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: nil, trailing: counter.leadingAnchor, paddingTop: 15, paddingLeading: 15, paddingBottom: 0, paddingTrailing: 15, width: nil, height: nil)
        
        priceLabel.anchor(top: itemLabel.bottomAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, paddingTop: 5, paddingLeading: 15, paddingBottom: 15, paddingTrailing: 15, width: nil, height: nil)
    }
}
