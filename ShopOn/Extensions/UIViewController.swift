//
//  UIViewController.swift
//  ShopOn
//
//  Created by Benjamin on 27/12/2018.
//  Copyright © 2018 Envision. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showErrorAlert(_ message: String?){
        let alertPrompt = UIAlertController(title: NSLocalizedString("voiceOver_oops", comment: ""), message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: NSLocalizedString("voiceOver_Cancel", comment: ""), style: .cancel)
        alertPrompt.addAction(cancel)
        self.present(alertPrompt, animated: true, completion: nil)
    }
}
