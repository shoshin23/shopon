//
//  UIView+Additions.swift
//  ShopOn
//
//  Created by Ilya Kuznetsov on 3/21/19.
//  Copyright © 2019 Envision. All rights reserved.
//

import UIKit

extension UIView {
    
    func setupShadow() {
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.15
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
